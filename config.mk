FIRMWARE_IMAGES := $(wildcard vendor/xiaomi/redwood-firmware/images/*)

AB_OTA_PARTITIONS += \
    $(foreach f, $(notdir $(FIRMWARE_IMAGES)), $(basename $(f)))
